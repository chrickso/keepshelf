class CreateFolders < ActiveRecord::Migration[5.2]
  def change
    create_table :folders do |t|
      t.string :account_id, :null => false
      t.string :name, :null => false
      t.text :description
      t.string :slug, :null => false

      t.timestamps
    end
    add_index :folders, :account_id
    add_index :folders, [:account_id, :slug], unique:true
  end
end
