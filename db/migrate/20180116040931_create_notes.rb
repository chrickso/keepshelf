class CreateNotes < ActiveRecord::Migration[5.2]
  def change
    create_table :notes, id: :uuid do |t|
      t.string :title
      t.text :body
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :notes, [:user_id, :created_at]
  end
end
