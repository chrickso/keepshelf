class CreatePermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :permissions do |t|
      t.integer :account_id
      t.integer :user_id
      t.string :resource
      t.string :role

      t.timestamps
    end
    add_index :permissions, [:account_id, :user_id], unique: true
  end
end
