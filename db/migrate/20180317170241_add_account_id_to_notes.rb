class AddAccountIdToNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :account_id, :integer, null: false
  end
end
