class AddAncestryToFolder < ActiveRecord::Migration[5.2]
  def change
    add_column :folders, :ancestry, :string
    add_index :folders, [:account_id, :ancestry]
  end
end
