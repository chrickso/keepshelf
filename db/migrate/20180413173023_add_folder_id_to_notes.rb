class AddFolderIdToNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :folder_id, :integer
    add_index :notes, [:account_id, :folder_id]
  end
end