class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :hash_id
      t.integer :owner_id

      t.timestamps
    end
    add_index :accounts, :hash_id, unique: true
    add_index :accounts, :owner_id
  end
end
