# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Users

User.create!(#name:  "Chris",
             email: "chrickso@gmail.com",
             password:              "Dratini11",
             password_confirmation: "Dratini11",
             #admin: true,
             #activated: true,
             #activated_at: Time.zone.now)
             )

User.create!(#name:  "Chris",
             email: "chrickso2@gmail.com",
             password:              "Dratini11",
             password_confirmation: "Dratini11",
             #admin: true,
             #activated: true,
             #activated_at: Time.zone.now)
             )

Permission.create!(user_id: 2,
                   account_id: 1,
                   resource: "account",
                   role: :admin)