class NotePolicy < ApplicationPolicy

  def index?
    Permission.where({account_id: @params_account_id, user_id: user.id}) \
    .where({resource: 'account', role: :owner}) \
    .or(Permission.where({account_id: @params_account_id, user_id: user.id}) \
        .where({resource: 'account', role: :admin})) \
    .exists?
  end
  
  def show?
    Permission.where({account_id: record.account_id, user_id: user.id}) \
    .where({resource: 'account', role: :owner}) \
    .or(Permission.where({account_id: record.account_id, user_id: user.id}) \
        .where({resource: 'account', role: :admin})) \
    .or(Permission.where({account_id: record.account_id, user_id: user.id}) \
        .where({resource: 'note', role: :viewer})) \
    .exists?
  end

  def create?
    Permission.where(account_id: record.account_id).where(user_id: user.id) \
    .where({resource: 'account', role: :owner}) \
    .or(Permission.where({account_id: @params_account_id, user_id: user.id}) \
        .where({resource: 'account', role: :admin})) \
    .exists?
  end

  def update?
    Permission.where(account_id: record.account_id).where(user_id: user.id) \
    .where({resource: 'account', role: :owner}).exists?
  end

  def destroy?
    Permission.where(account_id: record.account_id).where(user_id: user.id) \
    .where({resource: 'account', role: :owner}).exists?
  end

end



# Permission.where(account_id: record.account_id).where(user_id: user.id).exists?