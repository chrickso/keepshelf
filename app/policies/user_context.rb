class UserContext
  attr_reader :user, :params_account_id

  def initialize(user, params_account_id)
    @user = user
    @params_account_id = params_account_id
  end
end