class UserPolicy < ApplicationPolicy

  def show?
    Permission.where({account_id: @params_account_id, user_id: user.id}) \
    .where({resource: 'account', role: :owner}).exists?
  end

end



# Permission.where(account_id: record.account_id).where(user_id: user.id).exists?