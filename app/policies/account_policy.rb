class AccountPolicy < ApplicationPolicy

  def home?
    Permission.where({account_id: @params_account_id, user_id: user.id}) \
    .where({resource: 'account', role: :owner}) \
    .or(Permission.where({account_id: @params_account_id, user_id: user.id}) \
        .where({resource: 'account', role: :admin})) \
    .exists?
  end

end