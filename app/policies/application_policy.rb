class ApplicationPolicy
  attr_reader :account_id, :user, :record

  def initialize(user_context, record)
    @user = user_context.user
    @params_account_id = user_context.params_account_id
    @record = record
  end

  def index?
    false
  end

  def home?
    false
  end

  def show?
    # scope.where(:id => record.id).exists?
    false
  end

  def create?
    false
  end

  def new?
    create?
  end

  def update?
    false
  end

  def edit?
    update?
  end

  def destroy?
    false
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
