class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account
  # before_action :correct_user, only: [:home, :show]

  def home
    @note = Note.new
    @notes = @params_account.notes.all
  end

  def show
    authorize current_user
  end
end
