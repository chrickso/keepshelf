class ApplicationController < ActionController::Base

  # pundit
    include Pundit

    # https://medium.freecodecamp.org/rails-authorization-with-pundit-a3d1afcb8fd2
    def pundit_user
      UserContext.new(current_user, @params_account.id)
    end

    # some code snippets taken directly from
    # https://blog.carbonfive.com/2013/10/21/migrating-to-pundit-from-cancan/

    # Verify that controller actions are authorized. Optional, but good.
    after_action :verify_authorized,  except: :index, unless: :devise_controller?
    # after_action :verify_authorized, unless: :devise_controller?
    # after_action :verify_policy_scoped, only: :index, unless: :devise_controller?

    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized


  # devise
    before_action :configure_permitted_parameters, if: :devise_controller?

    # devise custom layouts
    # https://github.com/plataformatec/devise/wiki/how-to:-create-custom-layouts#applicationcontroller
    layout :layout_by_resource

    # for devise 'redirect to current page after login/logout'
    # https://github.com/plataformatec/devise/wiki/How-To:-Redirect-back-to-current-page-after-sign-in,-sign-out,-sign-up,-update
    before_action :store_user_location!, if: :storable_location?

    def after_sign_in_path_for(resource_or_scope)
      stored_location_for(resource_or_scope) || super
    end

    def after_sign_out_path_for(resource_or_scope)
      stored_location_for(resource_or_scope) || super
    end

    def set_account
      account_id = params[:account_id]
      # routes require the account_id is an 8-digit integer or else 404
      @params_account = Account.find_by!(hash_id: account_id)
    end


  protected

    # devise
    def configure_permitted_parameters
      added_attrs = [:username, :email, :password, :password_confirmation, :remember_me]
      devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
      devise_parameter_sanitizer.permit :account_update, keys: added_attrs
    end

  private

    # use different view layouts depending on resource being accessed
    def layout_by_resource
      if devise_controller?
        "devise/devise"
      elsif not current_user
        "static/static"
      else
        "application"
      end
    end


    # for storing users last location to return to after logging int
      # Its important that the location is NOT stored if:
      # - The request method is not GET (non idempotent)
      # - The request is handled by a Devise controller such as Devise::SessionsController as that could cause an 
      #    infinite redirect loop.
      # - The request is an Ajax request as this can lead to very unexpected behaviour.
      def storable_location?
        request.get? && is_navigational_format? && !devise_controller? && !request.xhr?
      end

      def store_user_location!
        # :user is the scope we are authenticating
        store_location_for(:user, request.fullpath)
      end

    # pundit
      def user_not_authorized
        # check if user logged in and if so go directly to the account home to prevent multiple redirects
        # current_user ? redirect_path = account_home_url(account_id: current_user.account.hash_id) : root_path
        # flash[:error] = "You are not authorized to perform this action."
        # redirect_to request.headers["Referer"] || redirect_path
        render file: "#{Rails.root}/public/403", status: :forbidden
      end

end
