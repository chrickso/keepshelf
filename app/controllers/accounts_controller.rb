class AccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account

  def home
    authorize Account
  end

end
