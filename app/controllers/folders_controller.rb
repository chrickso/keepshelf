class FoldersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account
  before_action :set_folder, only: [ :show, :edit, :update ]

  def index
    authorize Folder
    @folders = @params_account.folders.all
    @folder = @params_account.folders.new
    # @params_folder = Folder.friendly.find_by!(account: @params_account.id, name: 'root')
  end

  def show
    authorize @params_folder
    @folder = @params_account.folders.new
    @note = @params_account.notes.new
    @notes = @params_account.notes.where(folder_id: @params_folder.id)
  end

  def new
    @folder = @params_account.folders.new
    authorize @folder
  end


  def edit
    @folder = @params_folder
    authorize @folder
  end


  def create
    @folder = @params_account.folders.build(folder_params)

    authorize @folder

    if @folder.save
      # redirect_to redirect_to_parent_folder_if_present_else_notes_index
      redirect_to folder_path(@params_account.hash_id, @folder.slug), flash: { success: "Folder was successfully created." }
    else
      # @folders = @params_account.folders.all
      # flash.now[:error] = "Folder not saved. See error(s) below."
      # render :index
      respond_to do |format|
        format.js
      end
    end
  end


  def update
    @folder = @params_folder
    authorize @folder
    if @folder.update!(folder_params)
      redirect_to folder_path(@params_account.hash_id, @folder.slug, anchor: @folder.slug), notice: 'Folder was successfully updated.'
    else
      respond_to do |format|
        format.js
      end
    end
  end


  def destroy
    authorize @folder
    @folder.destroy
    redirect_to folder_path(@params_account.hash_id, @folder.parent_id), notice: 'Folder was successfully destroyed.'
  end



  private

    def set_folder
      folder_id = params[:id]
      # verify account_id is an x-digit integer or else 404
      @params_folder = Folder.friendly.find_by!(account: @params_account.id, slug: folder_id)
    end

    def folder_params
      params.require(:folder).permit(:name, :parent_id)
    end

    def redirect_to_parent_folder_if_present_else_notes_index
      if @folder.parent.present?
        redirect_url = folder_path(@params_account.hash_id, @folder.parent.slug, anchor: @folder.slug)
      else
        redirect_url = notes_path(@params_account.hash_id, anchor: @folder.slug)
      end
    end

end
