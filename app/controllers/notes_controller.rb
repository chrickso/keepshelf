class NotesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account
  before_action :set_note, only: [:show, :edit, :update, :destroy]


  def index
    # @notes = Note.all
    authorize Note
    @folders = @params_account.folders.roots
    @folder = @params_account.folders.new
    @note = @params_account.notes.new
    @notes = @params_account.notes.where(folder_id: nil)
    # @notes = @params_account.notes.where(account_id: @params_account.id, folder_id: @params_folder.id)
    # authorize(@params_account, policy: NotePolicy)
  end


  def show
    authorize @note
  end


  def new
    @note = @params_account.notes.new
    authorize @note
  end


  def edit
    authorize @note
  end


  def create
    @note = @params_account.notes.build(note_params.merge(user_id: current_user.id))

    authorize @note

    if @note.save
      redirect_to helpers.redirect_to_notes_folder_if_present_else_note_index, flash: { success: "Note was successfully created." }
    else
      # need to ajax this form so you don't leave the folder#show and don't have this render on a different URL
      # @params_folder = @note.folder
      # @notes = @params_account.notes.all
      # flash.now[:error] = "Note not saved. See error(s) below."
      # render 'folders/show'
      respond_to do |format|
        format.js
      end
    end
  end


  def update
    authorize @note
    if @note.update(note_params.merge(user_id: current_user.id))
      redirect_to helpers.redirect_to_notes_folder_if_present_else_note_index, notice: 'Note was successfully updated.'
    else
      respond_to do |format|
        format.js
      end
    end
  end


  def destroy
    authorize @note
    @note.destroy
    redirect_to helpers.redirect_to_notes_folder_if_present_else_note_index, notice: 'Note was successfully destroyed.'
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.

    def set_note
      @note = @params_account.notes.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def note_params
      params.require(:note).permit(:folder_id, :title, :body)
    end

end
