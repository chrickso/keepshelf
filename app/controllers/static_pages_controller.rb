class StaticPagesController < ApplicationController
  before_action :redirect_to_account_home_if_logged_in

  # pundit
  skip_after_action :verify_authorized
  # skip_after_action :verify_policy_scoped

  def index
  end

  private

    def redirect_to_account_home_if_logged_in
      redirect_to account_home_path(current_user.account.hash_id) if current_user unless @params_account
    end
end
