module NotesHelper

  def redirect_to_notes_folder_if_present_else_note_index
    if @note.folder_id.present?
      if action_name == 'destroy'
        folder_path(@params_account.hash_id, @note.folder.slug)
      else
        folder_path(@params_account.hash_id, @note.folder.slug, anchor: @note.id)
      end
    else
      if action_name == 'destroy'
        notes_path(@params_account.hash_id)
      else
        notes_path(@params_account.hash_id, anchor: @note.id)
      end
    end
  end
  
end
