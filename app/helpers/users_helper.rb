module UsersHelper

  # returns true if the given user is the current user.
  def current_user?(user)
    user == current_user
  end

  private

    # redirects to current_user's home if accessing a different :user_id
    def correct_user
      @user = User.find(params[:user_id])
      redirect_to(users_home_path(current_user)) unless current_user?(@user)
    end

end
