class Folder < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: [ :sequentially_slugged, :scoped ], scope: :account_id
  
  has_ancestry

  belongs_to :account
  has_many :notes, dependent: :destroy

  validates :name, presence: true

  def should_generate_new_friendly_id?
    name_changed?
  end

end
