class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :account, foreign_key: "owner_id"
  has_many :permissions
  has_many :notes

  before_create :set_account
  after_create :set_default_permission_for_account_owner

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX }
                    # uniqueness: { case_sensitive: false }

  # @return [Boolean] user should be remembered when he logs in (with cookie)
  # so he won't be asked to login again
  # https://github.com/plataformatec/devise/issues/1513#issuecomment-40070855
  def remember_me
    (super == nil) ? '1' : super
  end

  private
  
    def set_account
      build_account unless account.present?
    end

    def set_default_permission_for_account_owner
      permissions.create(account_id: self.account.id, user_id: self.id,
                         resource: 'account', role: :owner)
    end    

end
