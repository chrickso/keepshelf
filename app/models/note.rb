class Note < ApplicationRecord
  belongs_to :account
  belongs_to :folder, optional: true
  belongs_to :user
  validates :title, presence: true, unless: :body?
  validates :body, presence: true, unless: :title?
  
end
