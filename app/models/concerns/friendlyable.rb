# https://hackernoon.com/how-to-use-hash-ids-in-your-url-in-ruby-on-rails-5-e8b7cdd31733
module Friendlyable
  extend ActiveSupport::Concern

  included do
    extend ::FriendlyId
    before_create :set_hash_id
    friendly_id :hash_id
  end

  def set_hash_id
    hash_id = nil
    loop do
      # hash_id = SecureRandom.urlsafe_base64(9).gsub(/-|_/,('a'..'z').to_a[rand(26)])
      # hash_id = rand(9 ** 9).to_s.rjust(8,'0') # https://stackoverflow.com/a/34582/523051
      # hash_id = '%08d' % rand(10 ** 8) # works perfect
      hash_id = Array.new(8) { rand(10) }.join
      break unless self.class.name.constantize.where(:hash_id => hash_id).exists?
    end
    self.hash_id = hash_id
  end

end