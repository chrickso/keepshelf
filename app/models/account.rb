class Account < ApplicationRecord
  include Friendlyable

  belongs_to :owner, class_name: "User"
  has_many :folders
  has_many :permissions
  has_many :notes

  # after_create :create_folder_root_for_account

  # private
  #   def create_folder_root_for_account
  #     root = folders.create(account_id: self.id, name: 'root')
  #     # folders.create(account_id: self.id, name: 'test', parent_id: root.id)
  #   end

end
