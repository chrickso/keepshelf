

// add a class to bootstrap alerts before turbolinks caches to prevent blinking flash messages
// per https://github.com/turbolinks/turbolinks/issues/309#issuecomment-365560315
document.addEventListener("turbolinks:before-cache", function() {
  $('.alert').addClass('hidden');
})