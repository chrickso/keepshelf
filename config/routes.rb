Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => "signup" }
  
  # https://stackoverflow.com/a/13219048
  # authenticated :user do
  #   root 'users#home', as: :authenticated_root
  # end
  root 'static_pages#index'

  get ':account_id/home', to: 'accounts#home', as: 'account_home', constraints: { account_id: /[0-9]{8}/ }
  get ':account_id/', to: redirect('%{account_id}/home'), constraints: { account_id: /[0-9]{8}/ }
  get ':account_id/my/profile', to: 'users#show', as: 'users_profile', constraints: { account_id: /[0-9]{8}/ }
  scope ':account_id', constraints: { account_id: /[0-9]{8}/ } do 
    resources :folders
    resources :notes
  end
  
end