require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @params_account = accounts(:account_one)
    @user = users(:user_one)
    @other_user = users(:user_two)
  end

  test "should get show if logged in" do
    sign_in @user
    get users_profile_url(account_id: @params_account.hash_id)
    assert_response :success
  end

  test "should render :forbidden if attempting to get other user's home without permission" do
    sign_in @user
    get account_home_url(account_id: @other_user.account.hash_id)
    assert_response :forbidden
  end

  test "should render :forbidden if attempting to get other user's profile" do
    sign_in @user
    get users_profile_url(account_id: @other_user.account.hash_id)
    assert_response :forbidden
  end

  test "should redirect to login if attempting to get a user's show when logged out" do
    get users_profile_url(@user.account.hash_id)
    assert_redirected_to new_user_session_path
  end

end
