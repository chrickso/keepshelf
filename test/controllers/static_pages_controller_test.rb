require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  test "should get static home of not signed in" do
    get root_path
    assert_select "h1", "StaticPages#home"
  end

end
