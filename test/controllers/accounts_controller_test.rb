require 'test_helper'

class AccountsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @params_account = accounts(:account_one)
    @other_account = accounts(:account_two)
    @user = users(:user_one)
    @other_user = users(:user_two)
  end

  test "should get account home if signed in as the accounts owner" do
    sign_in @user
    get account_home_url(@user.account.hash_id)
    assert_response :success
  end

  test "should render :forbidden if accessing another users home with no permissions" do
    sign_in @user
    get account_home_url(@other_user.account.hash_id)
    # assert_redirected_to account_home_url(account_id: @user.account.hash_id)
    assert_response :forbidden
  end

  test "should get account home if accessing another users home with permissions Account/:admin" do
    sign_in @other_user
    get account_home_url(@user.account.hash_id)
    assert_response :success
  end

  test "root should redirect to account home if logged in" do
    sign_in @user
    get root_path
    assert_response :redirect
    follow_redirect!
    assert_select 'h1', "Accounts#home"
  end

end
