require 'test_helper'

class NotesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @params_account = accounts(:account_one)
    @user = users(:user_one)
    @other_user = users(:user_two)
    @note_by_user_no_folder = notes(:note_in_account1_by_user1_no_folder)
    @note_by_user_with_folder = notes(:note_in_account1_by_user1_with_folder)
    @note_by_other_user_no_folder = notes(:note_in_account2_by_user2_no_folder)
  end

  # follow format
  # SHOULD <expected behavior> [AND <expected behavior>][(SINCE <clarify expected behavior>)] WHEN <some action> IF <something is true/false>

  # authenticated tests

    test "SHOULD redirect to user login WHEN gets Notes#index IF not logged in" do
      get notes_url(@user.account.hash_id)
      assert_redirected_to user_session_url
    end

    test "SHOULD redirect to user login WHEN gets Notes#show IF not logged in" do
      get note_url(@user.account.hash_id, @note_by_user_no_folder)
      assert_redirected_to user_session_url
    end

    test "SHOULD redirect to user login WHEN gets Notes#new IF not logged in" do
      get new_note_url(@user.account.hash_id)
      assert_redirected_to user_session_url
    end

    test "SHOULD redirect to user login WHEN gets Notes#edit IF not logged in" do
      get edit_note_url(@user.account.hash_id, @note_by_user_no_folder)
      assert_redirected_to user_session_url
    end

    test "SHOULD redirect to user login WHEN posts Notes#create IF not logged in" do
      post notes_url(@user.account.hash_id), params: { note: { body: @note_by_user_no_folder.body } }
      assert_redirected_to user_session_url
    end

    test "SHOULD redirect to user login WHEN patches Notes#update IF not logged in" do
      patch note_url(@user.account.hash_id, @note_by_user_no_folder), params: { note: { body: @note_by_user_no_folder.body } }
      assert_redirected_to user_session_url
    end

    test "SHOULD redirect to user login WHEN deletes Notes#destroy IF not logged in" do
      delete note_url(@user.account.hash_id, @note_by_user_no_folder), params: { note: { body: @note_by_user_no_folder.body } }
      assert_redirected_to user_session_url
    end

  # access to own account tests

    test "SHOULD response 200 WHEN gets users own Note#index IF logged in as account:owner" do
      sign_in @user
      get notes_url(@user.account.hash_id)
      assert_response :success
    end

    test "SHOULD response 200 WHEN gets users own Notes#show IF logged in as account:owner" do
      sign_in @user
      get note_url(@user.account.hash_id, @note_by_user_no_folder)
      assert_response :success
    end

    test "SHOULD response 200 WHEN gets users own Notes#new IF logged in as account:owner" do
      sign_in @user
      get new_note_url(@user.account.hash_id)
      assert_response :success
    end

    test "SHOULD response 200 WHEN gets users own Notes#edit of note without a folder IF logged in as account:owner" do
      sign_in @user
      get edit_note_url(@user.account.hash_id, @note_by_user_no_folder)
      assert_response :success
    end

    test "SHOULD response 200 WHEN gets users own Notes#edit of note with a folder IF logged in as account:owner" do
      sign_in @user
      get edit_note_url(@user.account.hash_id, @note_by_user_with_folder)
      assert_response :success
    end

    test "SHOULD create note AND redirect to Notes#index(SINCE null folder_id) WHEN title and body is provided IF logged in as account:owner" do
      sign_in @user
      assert_difference('@user.account.notes.count', +1) do
        post notes_url(@user.account.hash_id), params: { note: { title: @note_by_user_no_folder.title, body: @note_by_user_no_folder.body } }
      end
      assert_redirected_to notes_url(@user.account.hash_id, anchor: controller.instance_variable_get(:@note).id)
    end

    test "SHOULD create note AND redirect to notes Folder#show WHEN title and body is provided IF logged in as account:owner" do
      sign_in @user
      assert_difference('@user.account.notes.count', +1) do
        post notes_url(@user.account.hash_id), params: { note: { title: @note_by_user_with_folder.title,
                                                                 body: @note_by_user_with_folder.body,
                                                                 folder_id: @note_by_user_with_folder.folder_id } }
      end
      assert_redirected_to folder_url(@user.account.hash_id, @note_by_user_with_folder.folder.slug, anchor: controller.instance_variable_get(:@note).id)
    end

    test "SHOULD create note AND redirect to Notes#index(SINCE null folder_id) WHEN only title is provided IF logged in as account:owner" do
      sign_in @user
      assert_difference('@user.account.notes.count', +1) do
        post notes_url(@user.account.hash_id), params: { note: { title: @note_by_user_no_folder.title } }
      end
      assert_redirected_to notes_url(@user.account.hash_id, anchor: controller.instance_variable_get(:@note).id)
    end

    test "SHOULD create note AND redirect to Notes#index(SINCE null folder_id) WHEN only body is provided IF logged in as account:owner" do
      sign_in @user
      assert_difference('@user.account.notes.count', +1) do
        post notes_url(@user.account.hash_id), params: { note: { body: @note_by_user_no_folder.body } }
      end
      assert_redirected_to notes_url(@user.account.hash_id, anchor: controller.instance_variable_get(:@note).id)
    end

    test "SHOULD not create note AND response has 2 errors WHEN title and body are empty IF logged in as account:owner" do
      sign_in @user
      assert_no_difference('@user.account.notes.count') do
        post notes_url(@user.account.hash_id), params: { note: { body: '', title: '' } }, xhr: true
      end
      assert_match '2 errors', response.body
    end

    test "SHOULD update note AND redirect to Notes#index(SINCE null folder_id) WHEN title and body is provided IF logged in as account:owner" do
      sign_in @user
      assert_no_difference('@user.account.notes.count') do
        patch note_url(@user.account.hash_id, @note_by_user_no_folder), params: { note: { title: @note_by_user_with_folder.title,
                                                                                          body: @note_by_user_with_folder.body } }
      end
      @note_by_user_no_folder.reload
      assert_match @note_by_user_no_folder.title, @note_by_user_with_folder.title
      assert_match @note_by_user_no_folder.body, @note_by_user_with_folder.body
      assert_redirected_to notes_url(@user.account.hash_id, anchor: @note_by_user_no_folder)
    end

    test "SHOULD update note AND redirect to notes Folder#show WHEN title and body is provided IF logged in as account:owner" do
      sign_in @user
      assert_no_difference('@user.account.notes.count') do
        patch note_url(@user.account.hash_id, @note_by_user_with_folder), params: { note: { title: @note_by_user_no_folder.title,
                                                                                            body: @note_by_user_no_folder.body} }
      end
      @note_by_user_with_folder.reload
      assert_match @note_by_user_with_folder.title, @note_by_user_no_folder.title
      assert_match @note_by_user_with_folder.body, @note_by_user_no_folder.body
      assert_redirected_to folder_url(@user.account.hash_id, @note_by_user_with_folder.folder.slug, anchor: @note_by_user_with_folder)
    end

    test "SHOULD not update note AND response has 2 errors WHEN title and body are changed to empty IF logged in as account:owner" do
      sign_in @user
      assert_no_difference('@user.account.notes.count') do
        patch note_url(@user.account.hash_id, @note_by_user_no_folder), params: { note: { title: '',
                                                                                          body: '' } }, xhr: true
      end
      assert_match '2 errors', response.body
    end

    test "SHOULD destroy note AND redirect to Notes#index(SINCE null folder_id) IF logged in as account:owner" do
      sign_in @user
      assert_difference('@user.account.notes.count', -1) do
        delete note_url(@user.account.hash_id, @note_by_user_no_folder)
      end
      assert_redirected_to notes_url(@user.account.hash_id)
    end

    test "SHOULD destroy note AND redirect to notes Folder#show IF logged in as account:owner" do
      sign_in @user
      assert_difference('@user.account.notes.count', -1) do
        delete note_url(@user.account.hash_id, @note_by_user_with_folder)
      end
      assert_redirected_to folder_url(@user.account.hash_id, @note_by_user_with_folder.folder.slug)
    end

  # access to other's accounts tests

    # other_user is user:account:admin access tests

      ## assumes @other_user has account:admin permission of @user's account ##

      test "SHOULD response 200 WHEN other_user gets users Notes#index IF permissions for user:account:admin" do
        sign_in @other_user
        get notes_url(@user.account.hash_id)
        assert_response :success
      end

      test "SHOULD response 200 WHEN other_user gets users Notes#show IF permissions for user:account:admin" do
        sign_in @other_user
        get note_url(@user.account.hash_id, @note_by_user_no_folder)
        assert_response :success
      end

    # :editor tests


    # :viewer tests


    # own:account:owner has no permissions to other_user:account tests

      ## assumes @user has no permissions besides being it's own account's :owner ##

      test "SHOULD response 403 WHEN user gets other_users Notes#index IF permissions for user:account:owner" do
        sign_in @user
        get notes_url(@other_user.account.hash_id)
        assert_response :forbidden
      end

      test "SHOULD response 403 WHEN user gets other_users Notes#show IF permissions for user:account:owner" do
        sign_in @user
        get note_url(@other_user.account.hash_id, @note_by_other_user_no_folder)
        assert_response :forbidden
      end


end
