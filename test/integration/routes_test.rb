require 'test_helper'

class RoutesTest < ActionDispatch::IntegrationTest

  # https://stackoverflow.com/a/28300379/523051

  test "account_id that is <8 digits should fail" do
    assert_raises ActionController::UrlGenerationError do
      get account_home_url(account_id: 1111111)
    end
  end

  test "account_id that is >8 digits should fail" do
    assert_raises ActionController::UrlGenerationError do
      get account_home_url(account_id: 111111111)
    end
  end

  test "account_id that contains a letter should fail" do
    assert_raises ActionController::UrlGenerationError do
      get account_home_url(account_id: "11111a111")
    end
  end

  test "account_id that contains 8 digits should not raise an exception" do
    assert_nothing_raised { get account_home_url(account_id: 11111111) }
  end

end
