require 'test_helper'

class AccountsHomeTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @params_account = accounts(:account_one)
    @user = users(:user_one)
    @other_user = users(:user_two)
  end

  test "accounts home area" do
    sign_in @user
    get account_home_url(@params_account.hash_id)

  end

end
