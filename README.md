
*Startup project for dev session
================================

start apps:
- postgres.app

terminal:
  cd dropbox/repositories/keepshelf


  testing panel:
  bundle exec guard
  

  # webpack dev server
  ./bin/webpack-dev-server
  
    yarn upgrade webpack-dev-server --latest



*Rails credentials
===============================

  # edit rails credentials

    rails credentials:edit

  # access encrypted credentials

    Rails.application.credentials.aws[:access_key_id]     # => "123"
    Rails.application.credentials.aws[:secret_access_key] # => "345"
    Rails.application.credentials.secret_key_base         # => "2fdea...

  # set the master.key as a heroku ENV

    heroku config:set RAILS_MASTER_KEY=<your-master-key-here>






*Gem Commands
================================








*Testing Autorun Setup (Copied from RailsTutorial.org) - Guard
================================

https://www.railstutorial.org/book/_single-page#sec-guard

  # install gems
  gem 'guard', '~> 2.14', '>= 2.14.2'
  gem 'guard-minitest', '~> 2.4', '>= 2.4.6'

  run terminal commands:

    - bundle
    - bundle exec guard init

  copy over contents of RailsTutorial Guardfile:
    https://bitbucket.org/railstutorial/sample_app_4th_ed/raw/289fcb83f1cd72b51c05fe9319277d590d51f0d2/Guardfile

  The Spring server is still a little quirky as of this writing, and sometimes Spring processes will accumulate and slow performance of your tests. If your tests seem to be getting unusually sluggish, it’s thus a good idea to inspect the system processes and kill them if necessary

    To see all the processes on your system:

      - ps aux

    To filter the processes by type, you can run the results of ps through the grep pattern-matcher using a Unix pipe |:

      - ps aux | grep spring

    Use the kill command to issue the Unix termination signal (which happens to be 15) to the pid:

      - kill -15 12241

    Sometimes it’s convenient to kill all the processes matching a particular process name:

      - spring stop
      - pkill -15 -f spring

    Kill puma when it is hanging at "Gracefully stopping"

      - pkill -9 -f puma



# to find and kill all postgres connections that error out rails db:migrate:reset
sudo kill -9 `ps -u postgres -o pid`



This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...





# url notes

https://www.keepshelf.com/48796875/home                                       # user homepage
https://www.keepshelf.com/48796875/profile
https://www.keepshelf.com/48796875/settings
https://www.keepshelf.com/48796875/account
https://www.keepshelf.com/48796875/account/billing

https://www.keepshelf.com/48796875/folders/                                   # folder index
https://www.keepshelf.com/48796875/folder/stuff_2                             # folder show

https://www.keepshelf.com/48796875/notes/                                     # note index
https://www.keepshelf.com/48796875/note/12587456/note-slug-matters-not        # note show
https://www.keepshelf.com/48796875/note/12587456/note-slug-matters-not/edit   # note edit

https://www.keepshelf.com/48796875/bookmarks/                                 # bookmark index
https://www.keepshelf.com/48796875/history/                                   # history

https://www.keepshelf.com/48796875/tags/                                      # tag index
https://www.keepshelf.com/48796875/tag/tag-name-here                          # tag show

https://www.keepshelf.com/48796875/search/all/search-term-here                # search








# random notes

resources :microposts,          only: [:create, :destroy]

id:int creator_id:int folder_name:text folder_slug:text 